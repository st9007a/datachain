#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "block.h"
#include "crypto/sha256.h"

#define CHECK(cond, success_msg, fail_msg) \
    do {                           \
        if (cond) {                \
            printf(success_msg);   \
        } else {                   \
            printf(fail_msg);      \
            exit(1);               \
        }                          \
    } while(0);

void test_genesis_block()
{
    Block a = GENESIS_BLOCK;
    Block b = { 0, "", "", time(NULL), { "genesis", 7 } };

    CHECK(a.no == b.no, "test_genesis_block() no successful\n", "test_genesis_block() no failed\n");
    CHECK(memcmp(a.hash, b.hash, 32) == 0, "test_genesis_block() hash successful\n", "test_genesis_block() hash failed\n");
    CHECK(memcmp(a.prev_hash, b.prev_hash, 32) == 0, "test_genesis_block() prev_hash successful\n", "test_genesis_block() prev_hash failed\n");
    CHECK(memcmp(a.data.message, b.data.message, a.data.length) == 0, "test_genesis_block() message successful\n", "test_genesis_block() messagefailed\n");
    CHECK(a.data.length == b.data.length, "test_genesis_block() datalen successful\n", "test_genesis_block() datalen failed\n");
}

void test_valid_genesis_block()
{
    Block a = GENESIS_BLOCK;
    CHECK(is_valid_genesis_block(&a), "test_valid_genesis_block() successful\n", "test_genesis_block() failed\n");
}

void test_calc_hash()
{
    Block a = GENESIS_BLOCK;
    BYTE check[32];
    BYTE message[4 + 32 + sizeof(time_t) + a.data.length];

    memcpy(message, &a.no, sizeof(int));
    memcpy(message + 4, a.prev_hash, 32);
    memcpy(message + 36, &a.ts, sizeof(time_t));
    memcpy(message + 36 + sizeof(time_t), a.data.message, a.data.length);

    calc_hash(a.hash, &a);
    sha256_encode(check, message, 4 + 32 + sizeof(time_t) + a.data.length);

    CHECK(memcmp(check, a.hash, 32) == 0, "test_calc_hash() successful\n", "test_calc_hash() failed\n");
}

void test_generate_next_block()
{
    Block a = GENESIS_BLOCK;
    Payload data = { "test_message", 12 };

    Block *next = generate_next_block(&a, &data);

    CHECK(next->no == 1, "test_generate_next_block() no successful\n", "test_generate_next_block() no failed\n");
    CHECK(memcmp(a.hash, next->prev_hash, 32) == 0, "test_generate_next_block() hash successful\n", "test_generate_next_block() hash failed\n");
    CHECK(memcmp(next->data.message, data.message, 12) == 0, "test_generate_next_block() message successful\n", "test_generate_next_block() message failed\n");
    CHECK(next->data.length == data.length, "test_generate_next_block() datalen successful\n", "test_generate_next_block() datalen failed\n");

    free(next);
}

void test_validate_block()
{
    Block a = GENESIS_BLOCK;
    Payload data = { "test_message", 12 };
    Block *next = generate_next_block(&a, &data);

    CHECK(validate_block(next, &a), "test_validate_block() successful\n", "test_validate_block() failed\n");

    free(next);
}

void test_init_blockchain()
{
    BlockChain chain;

    init_blockchain(&chain, 3);

    CHECK(chain.space == 3, "test_init_blockchain() space successful\n", "test_init_blockchain() space failed\n");
    CHECK(chain.size == 0, "test_init_blockchain() size successful\n", "test_init_blockchain() size failed\n");
}

void test_add_block()
{
    BlockChain chain;
    Block a, b;

    init_blockchain(&chain, 1);
    add_block(&chain, &a);
    add_block(&chain, &b);

    CHECK(memcmp(&a, &chain.blocks[0], sizeof(Block)) == 0, "test_add_block() element0 successful\n", "test_add_block() element0 failed\n")
    CHECK(memcmp(&b, &chain.blocks[1], sizeof(Block)) == 0, "test_add_block() element1 successful\n", "test_add_block() element1 failed\n")
}

void test_get_block()
{
    BlockChain chain;
    Block a, b;

    init_blockchain(&chain, 1);
    add_block(&chain, &a);
    add_block(&chain, &b);

    Block c = get_block(&chain, 0),
          d = get_block(&chain, 1);

    CHECK(memcmp(&a, &c, sizeof(Block)) == 0, "test_get_block() element0 successful\n", "test_add_block() element0 failed\n")
    CHECK(memcmp(&b, &d, sizeof(Block)) == 0, "test_get_block() element1 successful\n", "test_add_block() element1 failed\n")
}

int main()
{
    test_genesis_block();
    test_valid_genesis_block();
    test_calc_hash();
    test_generate_next_block();
    test_validate_block();
    test_init_blockchain();
    test_add_block();
    test_get_block();

    return 0;
}
