#ifndef SHA256_H
#define SHA256_H

#include <stddef.h>

typedef unsigned char BYTE;
typedef unsigned int  WORD;

typedef struct __SHA256_CTX {
    BYTE data[64];
    WORD hash[8];
    WORD datalen;
    unsigned long bitlen;
} SHA256Ctx;

void sha256_encode(BYTE *dest, const BYTE *message, size_t length);

#endif
