#ifndef BLOCK_H
#define BLOCK_H

#include <stddef.h>
#include <time.h>

#define GENESIS_BLOCK \
    { 0, "", "", time(NULL), { "genesis", 7 } }

typedef unsigned char BYTE;

typedef struct __PAYLOAD {
    BYTE message[1024];
    size_t length;
} Payload;

typedef struct __BLOCK {
    int no;
    BYTE hash[32];
    BYTE prev_hash[32];
    time_t ts;
    Payload data;
} Block;

typedef struct __BLOCK_CHAIN {
    Block *blocks;
    size_t size;
    size_t space;
} BlockChain;

void calc_hash(BYTE *dest, const Block *block);
Block *generate_next_block(const Block *prev, const Payload *data);
int validate_block(const Block *block, const Block *prev_block);
int is_valid_genesis_block(const Block *block);

void init_blockchain(BlockChain *chain, size_t init_space);
void add_block(BlockChain *chain, Block *block);
Block get_block(BlockChain *chain, size_t index);
int validate_blockchain(BlockChain *chain);
void choose_blockchain(BlockChain *self, BlockChain *new_chain);

#endif
