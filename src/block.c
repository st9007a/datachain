#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "block.h"
#include "crypto/sha256.h"

static BYTE *calc_hash_for_block(const Block *block)
{
    BYTE *hash = malloc(sizeof(BYTE) * 32);
    calc_hash(hash, block);

    return hash;
}

void calc_hash(BYTE *dest, const Block *block)
{
    // no(int) + prev_hash(BYTE[32]) + ts(time_t) + Payload
    BYTE message[4 + 32 + sizeof(time_t) + block->data.length];
    memcpy(message, &block->no, sizeof(int));
    memcpy(message + 4, block->prev_hash, 32);
    memcpy(message + 36, &block->ts, sizeof(time_t));
    memcpy(message + 36 + sizeof(time_t), block->data.message, block->data.length);

    sha256_encode(dest, message, sizeof(message));
}

Block *generate_next_block(const Block *prev, const Payload *data)
{
    Block *b = malloc(sizeof(Block));
    b->no = prev->no + 1;
    b->ts = time(NULL);

    memcpy(b->prev_hash, prev->hash, 32);
    memcpy(b->data.message, data->message, data->length);
    b->data.length = data->length;

    calc_hash(b->hash, b);

    return b;
}

int validate_block(const Block *block, const Block *prev_block)
{
    BYTE *check_hash = calc_hash_for_block(block);
    int flag = memcmp(check_hash, block->hash, 32) == 0;

    free(check_hash);
    return block->no == prev_block->no + 1 && memcmp(block->prev_hash, prev_block->hash, 32) == 0 && flag;
}

int is_valid_genesis_block(const Block *block)
{
    Block b = GENESIS_BLOCK;

    int pass = 1;

    pass &= block->no == b.no;
    pass &= memcmp(block->hash, b.hash, 32) == 0;
    pass &= memcmp(block->prev_hash, b.prev_hash, 32) == 0;
    pass &= memcmp(block->data.message, b.data.message, b.data.length) == 0;
    pass &= block->data.length == b.data.length;

    return pass;
}

void init_blockchain(BlockChain *chain, size_t init_space)
{
    chain->blocks = malloc(sizeof(Block) * init_space);
    chain->size = 0;
    chain->space = init_space;
}

void add_block(BlockChain *chain, Block *block)
{
    if (chain->size == chain->space) {
        chain->space <<= 1;
        chain->blocks = realloc(chain->blocks, sizeof(Block) * chain->space);
    }

    chain->blocks[chain->size++] = *block;
}

Block get_block(BlockChain *chain, size_t index)
{
    if (index >= chain->size) {
        printf("Index is out of range.\n");
        exit(1);
    }

    return chain->blocks[index];
}

int validate_blockchain(BlockChain *chain)
{
    if (!is_valid_genesis_block(chain->blocks)) {
        return 0;
    }

    for (int i = 1; i < chain->size; i++) {
        if (!validate_block(&chain->blocks[i], &chain->blocks[i - 1])) {
            return 0;
        }
    }

    return 1;
}

void choose_blockchain(BlockChain *self, BlockChain *new_chain)
{
    if (validate_blockchain(new_chain) && new_chain->size > self->size) {
        free(self->blocks);

        self->blocks = new_chain->blocks;
        self->size = new_chain->size;
        self->space = new_chain->space;
    } else {
        printf("Received blockchain invalid\n");
    }
}
